using System;
using System.Collections.Generic;


namespace EPAM_Task
{
    class Program
    {
        class MatrixMinor
        {
            public static int Height { get; set; }
            public static int Width { get; set; }
            private int[,] matrix;
            //Indexer;
            public int this[int i, int j]
            {
                get
                {
                    return matrix[i, j];
                }
                set
                {
                    matrix[i, j] = value;
                }
            }
            //Constructor;
            public MatrixMinor(int height, int width)
            {
                Height = height;
                Width = width;
                if (height == 2 && width == 2)
                {
                    throw new Exception("Can't count minor of matrix 2x2.");
                }

                matrix = new int[Height,Width];
            }
            //Set matrix with random values from 0 to 9;
            public void SetMatrix()
            {
                Random rand = new Random();
                for (int i = 0; i < Height; i++)
                {
                    for (int j = 0; j < Width; j++)
                    {
                        matrix[i, j] = (int)(rand.NextDouble() * 10);
                    }
                }
            }
            //Get data from current matrix after remooving k - row and n - column;
            public List<int> GetAfterRemovingRowAndColumn(int indexRow, int indexColumn, int height, int width,int[,] currentMatrix)
            {
                List<int> list = new List<int>();
                for (int k = 0; k < height; k++)
                {
                    for (int n = 0; n < width; n++)
                    {
                        if (!(k == indexRow || n == indexColumn))
                        {
                            list.Add(currentMatrix[k, n]);
                        }
                    }
                }
                return list;
            }
            //Counts determinant of 3x3 matrix;
            public int DetTwoByTwo(List<int> list,int height = 3, int width = 3)
            {
                int[,] det = new int[height-1, width-1];
                int index = 0;
                int mainDiagonal = 1;
                int secondDiagonal = 1;
                for (int k = 0; k < height - 1; k++)
                {
                    for (int n = 0; n < width - 1; n++)
                    {
                        det[k, n] = list[index];
                        index++;
                        if (k == n)
                        {
                            mainDiagonal *= det[k, n];
                        }
                        if (k != n)
                        {
                            secondDiagonal *= det[k, n];
                        }
                    }
                }
                return  mainDiagonal - secondDiagonal;
            }
            //Counts determinant of 4x4 matrix;
            public int DetThreeByThree(List<int> list)
            {
                int[,] det = new int[Height - 1, Width - 1];
                int index = 0;
                int result = 0;
                for (int k = 0; k < Height - 1; k++)
                {
                    for (int n = 0; n < Width - 1; n++)
                    {
                        det[k, n] = list[index];
                        index++;
                    }
                }
                for (int k = 0; k < Height - 1; k++)
                {
                    for (int n = 0; n < Width - 1; n++)
                    {
                        if (k == 0 && n < Width - 1)
                        {
                            result += Convert.ToInt32(Math.Pow(-1, k + n + 2)) * det[k, n] *
                                DetTwoByTwo(GetAfterRemovingRowAndColumn(k, n, Height - 1, Width - 1, det), Height - 1, Width - 1);
                        }
                    }
                }
                return result;
            }
            public int CountMinorByElement(int i, int j)
            {
                List<int> list = new List<int>();
                list = GetAfterRemovingRowAndColumn(i, j, Height, Width,matrix);
                int minor = 0;
                if (Height > 4 && Width > 4)
                {
                    throw new Exception("Size of matrix is too big to count a minor by element.");
                }
                if (Height != Width)
                {
                    throw new Exception("Can not count minor of this matrix, it is not square");
                }
                if (Height == 3)
                {
                    minor = DetTwoByTwo(list,Height,Width);
                }
                if(Height == 4)
                {
                    minor = DetThreeByThree(list);
                }
                    return minor;
            }
            public int CountMinorKOrder()
            {
                List<int> list = new List<int>();
                list.Add(matrix[0, 0]);
                list.Add(matrix[0, Width - 1]);
                list.Add(matrix[Height - 1, 0]);
                list.Add(matrix[Height - 1, Width - 1]);
                int minor = DetTwoByTwo(list);
                return minor;
            }
            public int CountPremiumMinor()
            {
                if (Height < 3)
                {
                    throw new Exception("Matrix is too small to count premium minor");
                }
                if (Height != Width)
                {
                    throw new Exception("Can not count minor of this matrix, it is not square");
                }
                List<int> list = new List<int>();
                for (int k = 0; k < 2; k++)
                {
                    for (int n = 0; n < 2; n++)
                    {
                        list.Add(matrix[k, n]);
                    }
                }
                return DetTwoByTwo(list);
            }
            public int Sum(params int[] minors)
            {
                int result = 0;
                foreach (int i in minors)
                {
                    result += i;
                }
                return result;
            }
            public int Multiply(params int[] minors)
            {
                int result = 1;
                foreach (int i in minors)
                {
                    result *= i;
                }
                return result;
            }
            public double Division(params int[] minors)
            {
                double result = minors[0];
                for(int i = 1; i < minors.Length; i++)
                {
                    if (minors[i] == 0)
                    {
                        throw new DivideByZeroException("Division by zero");
                    }
                    result /= minors[i];
                }
                return result;
            }
            
        }

        static void Main(string[] args)
        {
            MatrixMinor matrix = new MatrixMinor(4, 4);
            matrix.SetMatrix();
            Console.WriteLine("Matrix {0}x{1}", MatrixMinor.Height, MatrixMinor.Width);
            for (int i = 0; i < MatrixMinor.Height; i++)
            {
                for (int j = 0; j < MatrixMinor.Width; j++)
                {
                    Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
            try
            {
                Console.WriteLine("Minor of element ({1},{2}) = {0}, by e(i,j) element;", matrix.CountMinorByElement(0, 0), 0, 0);
                Console.WriteLine("Minor of element ({1},{2}) = {0}, by e(i,j) element;", matrix.CountMinorByElement(2, 2), 2, 2);
                Console.WriteLine("The result of division of minors = {0};", matrix.Division(matrix.CountMinorByElement(0, 0), matrix.CountMinorByElement(2, 2)));
                Console.WriteLine("The result of multiplication of minors = {0};", matrix.Multiply(matrix.CountMinorByElement(0, 0), matrix.CountMinorByElement(2, 2)));
                Console.WriteLine("The Sum of minors = {0};", matrix.Sum(matrix.CountMinorByElement(0, 0), matrix.CountMinorByElement(2, 2)));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                Console.WriteLine("Minor of k order = {0};", matrix.CountMinorKOrder());
                Console.WriteLine("Premium minor of k order minor = {0};", matrix.CountPremiumMinor());
            }
            Console.ReadLine();
        }
    }
}
